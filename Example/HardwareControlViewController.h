//
//  HardwareControlViewController.h
//  Example
//
//  Created by Daniele Poggi on 11/02/2020.
//  Copyright © 2020 SCLAK S.p.A. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HardwareControlViewController : UIViewController

@end

NS_ASSUME_NONNULL_END
