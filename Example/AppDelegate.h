//
//  AppDelegate.h
//  Example
//
//  Created by Daniele Poggi on 26/01/2017.
//  Copyright © 2017 SCLAK S.p.A. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

