//
//  ViewController.m
//  Example
//
//  Created by Daniele Poggi on 26/01/2017.
//  Copyright © 2017 SCLAK S.p.A. All rights reserved.
//

#import "LocksViewController.h"
#import <SclakSDK/SclakSDK.h>
#import "Toast.h"

@interface LocksViewController () <UITableViewDataSource>

@property (nonatomic, strong) NSArray *peripherals;

@end

@implementation LocksViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // retrieve the Singleton
    SclakSDK *sclak = [SclakSDK sharedInstance];        
        
    // activate the SDK with API key and secret
    [sclak provideAPIKey:@"1036233271538867328" APISecret:@"12989983164f1c6b3a7714f83b833c6a8d110f7a7a1f7f15225671b2192230ec"];
    
    // login user with API
    [sclak loginWithUsername:@"demo@sclak.com" password:@"Demo1234" callback:^(BOOL success, User *user, ResponseObject *failureObject) {
       
        if (success) {
            
            if (sclak.getPeripherals.count > 0) {
                [self reloadData];
            }
            
            // retrieve peripherals
            [sclak getPeripheralsCallback:^(BOOL success, NSArray<Peripheral> *peripherals, ResponseObject *failureObject) {
               
                if (success) {
                    
                    // populate UI
                    [self reloadData];
                }
                
            }];
        }
        
    }];
    
    // option: disable proximity check while opening the sclak lock
    [SCKPeripheralUsageManager sharedInstance].enableProximityCheck = NO;
    
    // option: disable Sclak SDK alerts for access control check
    [SCKPeripheralUsageManager sharedInstance].enableAlerts = NO;
}

- (void) reloadData {
    self.peripherals = SclakSDK.sharedInstance.getPeripherals;
    [self.tableView reloadData];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (!self.peripherals)
        return 0;
    return self.peripherals.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PeripheralCell"];
    Peripheral *model = self.peripherals[indexPath.row];
    cell.textLabel.text = model.name;
    cell.detailTextLabel.text = model.desc;
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    Peripheral *peripheral = self.peripherals[indexPath.row];
    [[SclakSDK sharedInstance] openPeripheral:peripheral evaluationCallback:^(BOOL success, Privilege *privilege, SclakActionError error, NSString *localizedErrorMessage) {
        if (success) {
            NSLog(@"evaluation of the privilege completed");
        } else {
            [self.view makeToast:localizedErrorMessage];            
        }
    } responseCallback:^(BOOL success, NSError *error) {
        if (success) {
            NSLog(@"open completed");
        } else {
            NSLog(@"open error: %@", error);
        }
    }];
}

@end
