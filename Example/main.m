//
//  main.m
//  Example
//
//  Created by Daniele Poggi on 26/01/2017.
//  Copyright © 2017 SCLAK S.p.A. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
