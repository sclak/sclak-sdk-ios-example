//
//  TagPresenceController.h
//  Example
//
//  Created by Daniele Poggi on 07/09/2018.
//  Copyright © 2018 SCLAK S.p.A. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TagPresenceController : UIViewController

@property (strong, nonatomic) IBOutlet UILabel *statusLabel;
@property (strong, nonatomic) IBOutlet UIButton *readTagButton;

- (IBAction) readTagAction:(id)sender;

@end
