//
//  TagPresenceController.m
//  Example
//
//  Created by Daniele Poggi on 07/09/2018.
//  Copyright © 2018 SCLAK S.p.A. All rights reserved.
//

#import "TagPresenceController.h"
#import <SclakSDK/SclakSDK.h>

@interface TagPresenceController ()

@property SclakTagPresencePeripheral *tagPresence;

@end

@implementation TagPresenceController

- (void) viewDidLoad {
    [super viewDidLoad];
    
    NSString *btcode = @"4CE2F11401C6";
    
    // get the peripheral
    Peripheral *peripheral = nil; // retrieve the peripheral like in LocksViewController or similar
    
    // set the peripheral secret
    // this is needed because the Tag Presence is an accessory and there is no auto-loading
    // of accessory secrects
    [[SecretManager getInstance] setSecretForBtcode:peripheral.btcode secret:peripheral.secretCode];
    
    // retrieve the Bluetooth Model that represents the Hardware
    self.tagPresence = (SclakTagPresencePeripheral*) [[PPLCentralManager sharedInstance] getOrRestorePeripheralWithBtcode:btcode];
    
    // if the hardware has never been scanned once, it won't be available
    // then, after being scanned, this variable is always available even if the scanner is turned off.
    if (!self.tagPresence) {
        NSLog(@"Peripheral not available. turn on BLE scanner like in LocksController");
    }
}

- (void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    // disconnect when needed
    [self.tagPresence disconnect];
}

- (IBAction) readTagAction:(id)sender {
    
    // you can connect here in viewDidLoad and keep connection indefinitively
    // to do so, you need to disable auto-disconnection after 10 seconds of wait time
    self.tagPresence.inhibitAutoDisconnect = YES;
    
    // connect to the peripheral
    [self.tagPresence connectAuthCallback:^(BOOL success, PPLDiscoveredPeripheral *peripheral, NSError *error) {
        if (success) {
            
            // call the read tag command
            [self.tagPresence readTagCallback:^(BOOL success, NSException *ex) {
                
                // the information is available on the model
                [self reloadTagStatus];
            }];
        } else {
            
            NSLog(@"tag presence connection and/or authentication failed with exeption: %@", error);
        }
    }];
}

/**
 * read the SclakTagPresencePeripheral model and retrieve the information
 * - is the tag/badge on the reader?
 * ---> Tag_undetected: NO
 * ---> Tag_detected_invalid_code: There a tag on the reader, the tecnology is compatible (Mifare Classic), but it's not a SCLAK tag/badge
 * ---> Tag_detected: YES
 * -------> also, you can read the unique tag/badge pin code
 */
- (void) reloadTagStatus {
    switch (self.tagPresence.status) {
        default:
        case Tag_undetected:
            self.statusLabel.text = @"Undetected";
            break;
        case Tag_detected_invalid_code:
            self.statusLabel.text = @"Detected, invalid code";
            break;
        case Tag_detected:
            self.statusLabel.text = @"Detected";
            NSLog(@"the tag/badge code is: %@", self.tagPresence.pinCode);
            break;
    }
}

@end
