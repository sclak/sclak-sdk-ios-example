//
//  SclakFleetController.m
//  Example
//
//  Created by Daniele Poggi on 07/09/2018.
//  Copyright © 2018 SCLAK S.p.A. All rights reserved.
//

#import "SclakFleetController.h"
#import <SclakSDK/SclakSDK.h>

@interface SclakFleetController ()

@property AutomotivePeripheral *sclakFleet;

@end

@implementation SclakFleetController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSString *btcode = @"....";
    
    // 1. OFFLINE ACCESS CONTROL
    
    // you can check if the access/usage of peripheral can be executed "now" with PeripheralUsageManager
    
    SCKPeripheralUsageManager *usageManager = [SCKPeripheralUsageManager sharedInstance];
    
    // get the peripheral you need to check
    Peripheral *peripheral = nil; // get you peripheral like in LocksController
    
    // you need to build the input of PeripheralUsageManager, a new PeripheralUsage class
    SCKPeripheralUsage *usage = [SCKPeripheralUsage new];
    usage.peripheral = peripheral;
    
    // then you can execute a check
    [usageManager evaluatePeripheralUsage:usage callback:^(BOOL success, Privilege *privilege, SclakActionError error, NSString *localizedErrorMessage) {
        
        // success: if true, you can connect with the sclak fleet and continue the operation
        // privilege: the model representing why the usage can be made now
        // error: if success is false, error represents the first limit that doesn't allow usage
        // localizedStringMessage: a localized text representation of error, available when success is false
        
        if (success) {
            
            // 2. SCLAK FLEET CONNECTION AND USAGE
            
            // set the peripheral secret
            [[SecretManager getInstance] setSecretForBtcode:peripheral.btcode secret:peripheral.secretCode];
            
            self.sclakFleet = (AutomotivePeripheral*) [[PPLCentralManager sharedInstance] getOrRestorePeripheralWithBtcode:btcode];
            
            if (!self.sclakFleet) {
                NSLog(@"Peripheral not available. turn on BLE scanner like in LocksController");
            }
            
            // you can connect here in viewDidLoad and keep connection indefinitively
            // to do so, you need to disable auto-disconnection after 10 seconds of wait time
            self.sclakFleet.inhibitAutoDisconnect = YES;
            
            // you can avoid using the callbacks if you don't need them
            [self.sclakFleet connectAuthCallback:nil];
        }
        else {
            
            NSLog(@"sclak fleet cannot be used now because: %@", localizedErrorMessage);
        }
        
    }];
}

- (void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    // disconnect when needed
    [self.sclakFleet disconnect];
}

- (IBAction) unlockDoorsAction:(id)sender {
    
    // you can check if peripheral is ready to receive commands
    if (!self.sclakFleet.isAuthenticated) {
        return;
    }
    
    [self.sclakFleet unlockDoorCallback:^(BOOL success, NSError *error) {
        
    }];
}

- (IBAction) lockDoorsAction:(id)sender {
    
    // you can also pass nil for the callback if you don't use it
    [self.sclakFleet lockDoorCallback:nil];
}

- (IBAction) unlockImmobilizerAction:(id)sender {
    
    [self.sclakFleet unlockImmobilizerCallback:nil];
}

- (IBAction) lockImmobilizerAction:(id)sender {
    
    [self.sclakFleet lockImmobilizerCallback:nil];
}
@end
