//
//  SclakFleetController.h
//  Example
//
//  Created by Daniele Poggi on 07/09/2018.
//  Copyright © 2018 SCLAK S.p.A. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SclakFleetController : UIViewController

- (IBAction) unlockDoorsAction:(id)sender;
- (IBAction) lockDoorsAction:(id)sender;

- (IBAction) unlockImmobilizerAction:(id)sender;
- (IBAction) lockImmobilizerAction:(id)sender;

@end
