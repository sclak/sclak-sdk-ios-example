//
//  HardwareControlViewController.m
//  Example
//
//  Created by Daniele Poggi on 11/02/2020.
//  Copyright © 2020 SCLAK S.p.A. All rights reserved.
//

#import "HardwareControlViewController.h"
#import <SclakBle/SclakBle.h>

@interface HardwareControlViewController ()

@property (nonatomic, strong) SclakPeripheral *sclak;

@end

@implementation HardwareControlViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    /*
     this class demonstrate the communication with Sclak Hardware
     using only the module SclakBle
     */
    
    // get the peripheral btcode
    NSString *btcode = @"4CE2F1010000";
    
    // you need to manage the peripheral secret in some way
    // check other examples to do so
    // with an empty secret, the authentication will surely FAIL
    [[SecretManager getInstance] setSecretForBtcode:btcode secret:@""];
    
    // 1. get sclak instance
    self.sclak = (SclakPeripheral*) [[PPLCentralManager sharedInstance] getOrRestorePeripheralWithBtcode:btcode];
}

- (IBAction) readStatus:(id)sender {
    [self.sclak connectAuthCallback:^(BOOL success, PPLDiscoveredPeripheral *peripheral, NSError *error) {
            
        if (success) {
            
            [self.sclak readPortStatusCallback:^(BOOL success, NSError *error) {
                
                if (success) {
                    
                    NSLog(@"output status is: %@", self.sclak.port1Status); //1. port active, 0. inactive
                }
            }];
        }
    }];
}

- (IBAction) readGiviStatus:(id)sender {
    
    // get the peripheral btcode
    NSString *btcode = @"4CE2F11F0000";
    __weak GivikPeripheral *givi = (GivikPeripheral*) [[PPLCentralManager sharedInstance] getOrRestorePeripheralWithBtcode:btcode];
    
    [givi setGetInOutCallback:^(BOOL success, NSError *error) {
        
        // received a status change while connection is active
        
        BOOL sensorlockCloseStatus = givi.status.sensorlockCloseStatus;
        if (sensorlockCloseStatus) {
            NSLog(@"the lock is locked");
        }
        
        BOOL sensorlockOpenStatus = givi.status.sensorlockOpenStatus;
        if (sensorlockOpenStatus) {
            NSLog(@"the lock is unlocked");
        }
        
        // from firmware version >= 1.16
        BOOL hullOpenStatus = givi.status.hullOpenStatus;
        if (hullOpenStatus) {
            NSLog(@"the hull is %@", (hullOpenStatus ? @"open" : @"close"));
        }
        
        GivikStatus *status = givi.status;
        switch (status.lockStatus) {
            case GivikStatusUnknown: {
                NSLog(@"givi status unknown (not detected yet)");
                break;
            }
            case GivikStatusUnlocked: {
                NSLog(@"givi status unlocked");
                break;
            }
            case GivikStatusUnlocking: {
                NSLog(@"givi status unlocking");
                break;
            }
            case GivikStatusLocked: {
                NSLog(@"givi status locked");
                break;
            }
            case GivikStatusLocking: {
                NSLog(@"givi status locking");
                break;
            }
            case GivikStatusError: {
                NSLog(@"givi status error (lock is stuck by either a key or some internal objects)");
                break;
            }
            default:
                break;
        }
    }];
    
    [givi connectAuthCallback:^(BOOL success, PPLDiscoveredPeripheral *peripheral, NSError *error) {
        // + ~1s
        if (success) {
            
            [givi getInOutCallback:^(BOOL success, NSException *ex) {
                // + ~0.02s
                if (success) {
                    NSLog(@"request to listen to status change has been made");
                    NSLog(@"here givi.status is already available with the first value received");
                    NSLog(@"if you need all status changes, set the getInOutCallback ");
                }
                
            }];
        }
    }];
}

- (IBAction) detectTagsAction:(id)sender {
    
    NSString *btcode = @"4CE2F11F0000";
    __weak SclakReaderPeripheral *sclakReader = (SclakReaderPeripheral*) [[PPLCentralManager sharedInstance] getOrRestorePeripheralWithBtcode:btcode];
    
    [sclakReader connectAuthCallback:^(BOOL success, PPLDiscoveredPeripheral *peripheral, NSError *error) {
        if (success) {
                
            [sclakReader readTagCallback:^(BOOL success, NSException *ex) {
                
                NSArray *tags = sclakReader.tags;
                for (SclakReaderTag *tag in tags) {
                    
                    switch (tag.status) {
                        case Tag_undetected:
                        default: {
                            NSLog(@"tag undetected (impossible case)");
                            break;
                        }
                        case Tag_detected: {
                            NSLog(@"tag detected (OK status)");
                            break;
                        }
                        case Tag_detected_invalid_code: {
                            NSLog(@"tag detected invalid code (internal code hasn't been read correctly, retry)");
                            break;
                        }
                    }
                    
                    NSString *code = tag.pinCode;
                    NSLog(@"internal tag unique code: %@", code);
                }
                
            }];
        }
    }];
}

@end
