//
//  DataViewController.m
//  Example
//
//  Created by Daniele Poggi on 02/07/2018.
//  Copyright © 2018 SCLAK S.p.A. All rights reserved.
//

#import "DataViewController.h"
#import <SclakSDK/SclakSDK.h>
#import "Toast.h"

@interface DataViewController ()

@property IBOutlet UITableView *tableView;
@property SclakSDK *sclak;

@end

@implementation DataViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // the SDK can be setupped both on main thread and in background thread
    dispatch_queue_t queue = dispatch_queue_create("background", DISPATCH_QUEUE_SERIAL);
    dispatch_async(queue, ^{
        [self setupSDK];
    });
}

- (void) setupSDK {
    
    // get reference to SDK
    self.sclak = [SclakSDK sharedInstance];
    
    // FOR EXAMPLE PURPOSE ONLY
    // enable username and password for restoring the authentication token used in this example
    // F.username = @"testsdk@grr.la";
    // F.password = @"TestSDK2019";
    
    // enable the SDK
    NSString *projectKey = @"1036233271538867328";
    NSString *projectSecret = @"12989983164f1c6b3a7714f83b833c6a8d110f7a7a1f7f15225671b2192230ec";
    [self.sclak provideAPIKey:projectKey APISecret:projectSecret];
    
    // load Peripherals model from JSON
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"peripherals_with_guest_auth_token.json" ofType:nil];
    NSData *data = [NSData dataWithContentsOfFile:filePath];
    NSError *error = nil;
    Peripherals *peripherals = [[Peripherals alloc] initWithData:data error:&error];
    if (error) {
        NSLog(@"json deserialization error: %@", error);
    } else {
        
        // initialize SDK to work without a logged user
        // [self.sclak initializeWithoutUser];
        
        // here we inject Peripherals model to the SDK
        // the SDK is being emptied of all the locks and keys
        // and then the new content is saved
        [self.sclak setPeripherals:peripherals];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableView reloadData];
        });
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.sclak.getPeripherals.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DataTableCell" forIndexPath:indexPath];
    Peripheral *peripheral = self.sclak.getPeripherals[indexPath.row];
    if (peripheral.name) {
        cell.textLabel.text = peripheral.name;
    }
    if (peripheral.desc) {
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ - %@", peripheral.desc, peripheral.btcode];
    }
    if (!peripheral.name && !peripheral.desc) {
        cell.textLabel.text = peripheral.btcode;
    }
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    Peripheral *peripheral = self.sclak.getPeripherals[indexPath.row];
    [self.sclak openPeripheral:peripheral evaluationCallback:^(BOOL success, Privilege *privilege, SclakActionError error, NSString *localizedErrorMessage) {
        if (!success) {
            [self.view makeToast:localizedErrorMessage];
        }
    } responseCallback:^(BOOL success, NSError *error) {
        
    }];
}

@end
