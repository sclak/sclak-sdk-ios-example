//
//  GearViewController.h
//  Example
//
//  Created by Daniele Poggi on 27/11/2018.
//  Copyright © 2018 SCLAK S.p.A. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GearViewController : UIViewController

- (IBAction)openAction:(id)sender;
- (IBAction)closeAction:(id)sender;
- (IBAction)autoSetupAction:(id)sender;

@end
