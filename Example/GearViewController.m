//
//  GearViewController.m
//  Example
//
//  Created by Daniele Poggi on 27/11/2018.
//  Copyright © 2018 SCLAK S.p.A. All rights reserved.
//

#import "GearViewController.h"
#import <SclakSDK/SclakSDK.h>

@interface GearViewController () <SCKPeripheralUsageDelegate>

@property NSString *btcode;
@property SclakSDK *sclak;

@end

@implementation GearViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configureSclakSdk];
}

- (void) configureSclakSdk {
    self.sclak = [SclakSDK sharedInstance];
    [self.sclak initializeWithoutUser];
}

- (IBAction) openAction:(id)sender {
    [self openOrCloseButtonAction:true];
}

- (IBAction) closeAction:(id)sender {
    [self openOrCloseButtonAction:false];
}

- (IBAction) autoSetupAction:(id)sender {
    [self autoSetup];
}

#pragma mark - Actions

- (void) openOrCloseButtonAction:(BOOL)open {
    
    SCKPeripheralUsageManager *peripheralUsageManager = [SCKPeripheralUsageManager sharedInstance];
    
    Peripheral *peripheral = [self.sclak getPeripheralWithBtcode:self.btcode];
    
    if (!peripheral)
        return;
    
    SclakCommand command;
    LogUsageType usageType;
    if (open) {
        command = SclakCommandOpen;
        usageType = Access;
    } else {
        // you can close completely
        command = SclakCommandCloseFull;
        // you can also close only one time
        command = SclakCommandClose1Time;
        // or leave the choice to the Gear
        command = SclakCommandCloseAuto;
        
        usageType = Close;
    }
    
    SCKPeripheralUsage *usage = [SCKPeripheralUsage buildWithCommand:command
                                                           usageType:usageType
                                                          peripheral:peripheral
                                                            delegate:self
                                                          disconnect:YES];
    
    [peripheralUsageManager evaluateSclakUsage:usage startProgressCallback:^{
        
    } evaluationCallback:^(BOOL success, Privilege *privilege, SclakActionError error, NSString *localizedErrorMessage) {
        
    } responseCallback:^(BOOL success, NSError *error) {
        
    } disconnectCallback:^(BOOL success, NSException *ex) {
        
    }];
}

- (void) autoSetup {
    
    SclakGearAPeripheral *gearA = (SclakGearAPeripheral*) [[PPLCentralManager sharedInstance] getOrRestorePeripheralWithBtcode:self.btcode];
    
    [gearA connectAuthCallback:^(BOOL success, PPLDiscoveredPeripheral *peripheral, NSError *error) {
        if (success) {
            
            [gearA initializeCallback:^(BOOL success, NSException *ex) {
                
            }];
        }
    }];
}

@end
