//
//  ScanViewController.m
//  Example
//
//  Created by Daniele Poggi on 02/07/2018.
//  Copyright © 2018 SCLAK S.p.A. All rights reserved.
//

#import "ScanViewController.h"
#import <SclakSDK/SclakSDK.h>
#import "Toast.h"

@interface ScanViewController () <UITableViewDataSource, UITableViewDelegate>

@property IBOutlet UITableView *tableView;
@property PPLCentralManager *centralManager;

@end

@implementation ScanViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    __weak ScanViewController *selz = self;        
    
    // this singleton allows to manage the SCLAK BLE discovery
    // it has already been configured by the SDK to find every SCLAK nearby
    self.centralManager = [PPLCentralManager sharedInstance];
    
    // setup sclak framework options
    [self setupSclakFramework];
    
    // you can customize the scanner frequency (s)
    _centralManager.scannerFrequency = @1;
    
    // this block is called when the central manager changes state
    // for example when BLE is turned on/off
    _centralManager.onDidUpdateState = ^(PPLCentralManager *manager, CBManagerState state, CBManagerState previousState) {
        if (CBManagerStatePoweredOff == manager.state) {
            [selz.view makeToast:@"Hey! turn on the bluetooth !!!"];
        } else if (![manager scanning]) {
            // finally, start the BLE scanner
            [manager startScan];
        }
    };
    
    // this block is called when a peripheral is discovered
    // if isNewPeripheral is YES, it means that it's the first time the scanner finds it (since it was lost)
    _centralManager.onDidDiscoverPeripheral = ^(PPLDiscoveredPeripheral *peripheral, BOOL isNewPeripheral) {
        if (selz.isViewLoaded && selz.view.window) {
            [selz.tableView reloadData];
        }
    };
    
    // this block is called when one or more peripherals are lost
    _centralManager.onDidLostPeripherals = ^(NSArray *btcodes) {
        if (selz.isViewLoaded && selz.view.window) {
            [selz.tableView reloadData];
        }
    };
    
    // OR, you can use notifications!
    // if you register these, the blocks won't be called anymore (for optimization)
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(discoveryNotification:) name:kCentralManagerDidDiscoverPeripheralNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(proximityNotification:) name:kCentralManagerDidChangeProximityNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(lostNotification:) name:kCentralManagerDidLostPeripheralsNotification object:nil];
}

- (void) setupSclakFramework {
    
    // bluetooth sclak manager
    PPLCentralManager *centralManager = [PPLCentralManager sharedInstance];
    centralManager.monitorLostPeripherals = YES;
    centralManager.deviceIdentifiers = centralManager.allDeviceIdentifiers;
    centralManager.devicePreferences = @{
                                         kCentralModeBtcodeRootOption: @"4CE2F1",
                                         kCentralModeAutoScanOption: @NO,                    // scanner always on if possible (bluetooth turned on)
                                         kCentralModeScannerFrequencyOption: @0.1,           // scanner update frequency [0-10s] 0 = real-time
                                         kCentralModeSHA256ThresholdVersion: @3.0,
                                         kCentralModeAutoConnectOption: @NO,
                                         kCentralModeAutoAuthenticateOption: @YES,
                                         kCentralModeStopScanWhenConnectOption: @NO,         // should the scanner stop when connecting to a peripheral? then
                                         // when disconnected the scanner restarts if it was started before connection
                                         };
    [centralManager initialize];

}

- (void) viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    // don't forget to remove observers or this controller will continue to receive notifications!
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    // and you can also turn off the scanner when it's not needed
    [self.centralManager stopScan];
}

#pragma mark - Notification Center

/**
 * this notification is called when a peripheral is discovered
 */
- (void) discoveryNotification:(NSNotification*)notif {
    // you can do something with this
    PPLDiscoveredPeripheral *peripheral = notif.object;
    [self.tableView reloadData];
}

/**
 * this notification is called when a proximity change is detected
 */
- (void) proximityNotification:(NSNotification*)notif {
    [self.tableView reloadData];
}

/**
 * this notification is called when one or more peripherals are lost
 */
- (void) lostNotification:(NSNotification*)notif {
    // you can do something with this
    NSArray *lostPeripheralBtcodes = notif.object;
    
    [self.tableView reloadData];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.centralManager.discoveredBtcodes.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"BleScannerCell" forIndexPath:indexPath];
    NSString *btcode = self.centralManager.discoveredBtcodes[indexPath.row];
    cell.textLabel.text = btcode;
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *btcode = self.centralManager.discoveredBtcodes[indexPath.row];
    SclakSDK *sclak = [SclakSDK sharedInstance];
    Peripheral *peripheral = [sclak getPeripheralWithBtcode:btcode];
    if (peripheral) {
        [sclak openPeripheral:peripheral evaluationCallback:^(BOOL success, Privilege *privilege, SclakActionError error, NSString *localizedErrorMessage) {
            if (!success) {
                [self.view makeToast:localizedErrorMessage];
            }
        } responseCallback:^(BOOL success, NSError *error) {
            
        }];
    }
}

@end
