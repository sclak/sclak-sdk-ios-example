//
//  MenuViewController.m
//  Example
//
//  Created by Daniele Poggi on 02/07/2018.
//  Copyright © 2018 SCLAK S.p.A. All rights reserved.
//

#import "MenuViewController.h"
#import <SclakSDK/SclakSDK.h>

@interface MenuViewController ()

@end

@implementation MenuViewController

- (void) viewDidLoad {
    [super viewDidLoad];
    
    // SDK activation
    // these methods must be called before any other SDK methods
    
    // retrieve the Singleton for the first time
    // these allow it to initialize
    //SclakSDK *sclak = [SclakSDK sharedInstance];
    
    // activate the SDK with API key and secret
    //[sclak provideAPIKey:@"1036233271538867328" APISecret:@"12989983164f1c6b3a7714f83b833c6a8d110f7a7a1f7f15225671b2192230ec"];
}

@end
