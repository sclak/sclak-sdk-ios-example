
Sclak SDK Examples
=======================

This app contains examples about using the Sclak SDK in order to log-in to the Sclak Server, retrieve locks and keys, sync pin codes, access logs, battery statuses etc.


SDK Installation with Cocoapods
------------

1. choose the frameworks you need (ask for help if needed)
2. copy the required cocoapods dependencies in your project, e.g
pod 'SclakFoundation', :git => 'git@bitbucket.org:sclak/sclak-pods.git'
pod 'SclakBle', :git => 'git@bitbucket.org:sclak/sclak-pods.git'
3. pod install

SDK Installation without Cocoapods: build, embed and sign
------------

1. execute 'pod install'
2. build & run the example project on device target in Release
3. Copy the builded framework files from the example project dir to your app project directory
4. In your XCode project, import the frameworks
5. In your XCode Target, under General / Embedded Binaries, insert copied frameworks

How to obtain API key & API secret
----------------

You can request one API key/secret pair to your Sclak project contact 

Download Translations
-----------------

copy SclakSDK.strings in your project for your desired languages

